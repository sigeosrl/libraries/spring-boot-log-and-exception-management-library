package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ResponseStatus(NOT_ACCEPTABLE)
public class NotAcceptableException extends RuntimeException {
	public NotAcceptableException(String message) {
		super(message);
	}
	public NotAcceptableException(String message, Throwable cause) {
        super(message, cause);
    }
	public NotAcceptableException(Throwable cause) {
        super(cause);
    }
	public NotAcceptableException() {
        super();
    }
}
