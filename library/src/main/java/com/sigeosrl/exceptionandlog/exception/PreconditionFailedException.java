package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ResponseStatus(PRECONDITION_FAILED)
public class PreconditionFailedException extends RuntimeException {
	public PreconditionFailedException(String message) {
		super(message);
	}
	public PreconditionFailedException(String message, Throwable cause) {
        super(message, cause);
    }
	public PreconditionFailedException(Throwable cause) {
        super(cause);
    }
	public PreconditionFailedException() {
		super();
	}
}
