package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.EXPECTATION_FAILED;

@ResponseStatus(EXPECTATION_FAILED)
public class ExpectationFailedException extends RuntimeException {
	public ExpectationFailedException(String message) {
		super(message);
	}
	public ExpectationFailedException(String message, Throwable cause) {
        super(message, cause);
    }
	public ExpectationFailedException(Throwable cause) {
        super(cause);
    }
	public ExpectationFailedException() {
        super();
    }
}
