package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.PAYMENT_REQUIRED;

@ResponseStatus(PAYMENT_REQUIRED)
public class PaymentRequiredException extends RuntimeException {
	public PaymentRequiredException(String message) {
		super(message);
	}
	public PaymentRequiredException(String message, Throwable cause) {
        super(message, cause);
    }
	public PaymentRequiredException(Throwable cause) {
        super(cause);
    }
	public PaymentRequiredException() {
        super();
    }
}
