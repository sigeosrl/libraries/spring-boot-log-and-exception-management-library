package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.LOCKED;

@ResponseStatus(LOCKED)
public class LockedException extends RuntimeException {
	public LockedException(String message) {
		super(message);
	}
	public LockedException(String message, Throwable cause) {
        super(message, cause);
    }
	public LockedException(Throwable cause) {
        super(cause);
    }
	public LockedException() {
        super();
    }
}
