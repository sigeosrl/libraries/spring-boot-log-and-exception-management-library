package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ResponseStatus(LENGTH_REQUIRED)
public class LengthRequiredException extends RuntimeException {
	public LengthRequiredException(String message) {
		super(message);
	}
	public LengthRequiredException(String message, Throwable cause) {
        super(message, cause);
    }
	public LengthRequiredException(Throwable cause) {
        super(cause);
    }
	public LengthRequiredException() {
        super();
    }
}
