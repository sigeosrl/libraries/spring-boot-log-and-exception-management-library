package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@ResponseStatus(UNPROCESSABLE_ENTITY)
public class UnprocessableEntityException extends RuntimeException {
	public UnprocessableEntityException(String message) {
		super(message);
	}
	public UnprocessableEntityException(String message, Throwable cause) {
        super(message, cause);
    }
	public UnprocessableEntityException(Throwable cause) {
        super(cause);
    }
	public UnprocessableEntityException() {
		super();
	}
}
