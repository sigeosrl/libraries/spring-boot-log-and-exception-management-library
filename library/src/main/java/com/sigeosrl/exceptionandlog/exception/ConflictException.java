package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.CONFLICT;

@ResponseStatus(CONFLICT)
public class ConflictException extends RuntimeException {
	public ConflictException(String message) {
		super(message);
	}
	public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }
	public ConflictException(Throwable cause) {
        super(cause);
    }
	public ConflictException() {
        super();
    }
}
