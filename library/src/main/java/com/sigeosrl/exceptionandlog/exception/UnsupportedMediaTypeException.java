package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;

@ResponseStatus(UNSUPPORTED_MEDIA_TYPE)
public class UnsupportedMediaTypeException extends RuntimeException {
	public UnsupportedMediaTypeException(String message) {
		super(message);
	}
	public UnsupportedMediaTypeException(String message, Throwable cause) {
        super(message, cause);
    }
	public UnsupportedMediaTypeException(Throwable cause) {
        super(cause);
    }
	public UnsupportedMediaTypeException() {
        super();
    }
}
