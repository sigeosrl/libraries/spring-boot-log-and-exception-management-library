package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.FORBIDDEN;

@ResponseStatus(FORBIDDEN)
public class ForbiddenException extends RuntimeException {
	public ForbiddenException(String s) {
		super(s);
	}
	public ForbiddenException(String s, Throwable cause) {
		super(s, cause);
	}
	public ForbiddenException(Throwable cause) {
        super(cause);
    }
	public ForbiddenException() {
		super();
	}
}
