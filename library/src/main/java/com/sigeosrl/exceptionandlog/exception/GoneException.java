package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.GONE;

@ResponseStatus(GONE)
public class GoneException extends RuntimeException {
	public GoneException(String message) {
		super(message);
	}
	public GoneException(String message, Throwable cause) {
        super(message, cause);
    }
	public GoneException(Throwable cause) {
        super(cause);
    }
	public GoneException() {
        super();
    }
}
