package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.*;
import com.sigeosrl.exceptionandlog.exception.dto.DevErrorMessage;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
@Profile("dev")
public class DevExceptionHandler {
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(BAD_REQUEST)
    public DevErrorMessage handleBadRequestException(BadRequestException ex) {
        return new DevErrorMessage(BAD_REQUEST, ex.getMessage(), ex.getStackTrace(), BAD_REQUEST.value());
    }

    @ExceptionHandler(ConflictException.class)
    @ResponseStatus(CONFLICT)
    public DevErrorMessage handleConflictException(ConflictException ex) {
        return new DevErrorMessage(CONFLICT, ex.getMessage(), ex.getStackTrace(), CONFLICT.value());
    }

    @ExceptionHandler(ExpectationFailedException.class)
    @ResponseStatus(EXPECTATION_FAILED)
    public DevErrorMessage handleExpectationFailedException(ExpectationFailedException ex) {
        return new DevErrorMessage(EXPECTATION_FAILED, ex.getMessage(), ex.getStackTrace(), EXPECTATION_FAILED.value());
    }

    @ExceptionHandler(FailedDependencyException.class)
    @ResponseStatus(FAILED_DEPENDENCY)
    public DevErrorMessage handleFailedDependencyException(FailedDependencyException ex) {
        return new DevErrorMessage(FAILED_DEPENDENCY, ex.getMessage(), ex.getStackTrace(), FAILED_DEPENDENCY.value());
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(FORBIDDEN)
    public DevErrorMessage handleForbiddenException(ForbiddenException ex) {
        return new DevErrorMessage(FORBIDDEN, ex.getMessage(), ex.getStackTrace(), FORBIDDEN.value());
    }

    @ExceptionHandler(GoneException.class)
    @ResponseStatus(GONE)
    public DevErrorMessage handleGoneException(GoneException ex) {
        return new DevErrorMessage(GONE, ex.getMessage(), ex.getStackTrace(), GONE.value());
    }

    @ExceptionHandler(LengthRequiredException.class)
    @ResponseStatus(LENGTH_REQUIRED)
    public DevErrorMessage handleLengthRequiredException(LengthRequiredException ex) {
        return new DevErrorMessage(LENGTH_REQUIRED, ex.getMessage(), ex.getStackTrace(), LENGTH_REQUIRED.value());
    }

    @ExceptionHandler(LockedException.class)
    @ResponseStatus(LOCKED)
    public DevErrorMessage handleLockedException(LockedException ex) {
        return new DevErrorMessage(LOCKED, ex.getMessage(), ex.getStackTrace(), LOCKED.value());
    }

    @ExceptionHandler(NotAcceptableException.class)
    @ResponseStatus(NOT_ACCEPTABLE)
    public DevErrorMessage handleNotAcceptableException(NotAcceptableException ex) {
        return new DevErrorMessage(NOT_ACCEPTABLE, ex.getMessage(), ex.getStackTrace(), NOT_ACCEPTABLE.value());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public DevErrorMessage handleNotFoundException(NotFoundException ex) {
        return new DevErrorMessage(NOT_FOUND, ex.getMessage(), ex.getStackTrace(), NOT_FOUND.value());
    }

    @ExceptionHandler(PaymentRequiredException.class)
    @ResponseStatus(PAYMENT_REQUIRED)
    public DevErrorMessage handlePaymentRequiredException(PaymentRequiredException ex) {
        return new DevErrorMessage(PAYMENT_REQUIRED, ex.getMessage(), ex.getStackTrace(), PAYMENT_REQUIRED.value());
    }

    @ExceptionHandler(PreconditionFailedException.class)
    @ResponseStatus(PRECONDITION_FAILED)
    public DevErrorMessage handlePreconditionFailedException(PreconditionFailedException ex) {
        return new DevErrorMessage(PRECONDITION_FAILED, ex.getMessage(), ex.getStackTrace(), PRECONDITION_FAILED.value());
    }

    @ExceptionHandler(PreconditionRequiredException.class)
    @ResponseStatus(PRECONDITION_REQUIRED)
    public DevErrorMessage handlePreconditionRequiredException(PreconditionRequiredException ex) {
        return new DevErrorMessage(PRECONDITION_REQUIRED, ex.getMessage(), ex.getStackTrace(), PRECONDITION_REQUIRED.value());
    }

    @ExceptionHandler(TeapotException.class)
    @ResponseStatus(I_AM_A_TEAPOT)
    public DevErrorMessage handleTeapotException(TeapotException ex) {
        return new DevErrorMessage(I_AM_A_TEAPOT, ex.getMessage(), ex.getStackTrace(), I_AM_A_TEAPOT.value());
    }

    @ExceptionHandler(UnprocessableEntityException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public DevErrorMessage handleUnprocessableEntityException(UnprocessableEntityException ex) {
        return new DevErrorMessage(UNPROCESSABLE_ENTITY, ex.getMessage(), ex.getStackTrace(), UNPROCESSABLE_ENTITY.value());
    }

    @ExceptionHandler(UnsupportedMediaTypeException.class)
    @ResponseStatus(UNSUPPORTED_MEDIA_TYPE)
    public DevErrorMessage handleUnsupportedMediaTypeException(UnsupportedMediaTypeException ex) {
        return new DevErrorMessage(UNSUPPORTED_MEDIA_TYPE, ex.getMessage(), ex.getStackTrace(), UNSUPPORTED_MEDIA_TYPE.value());
    }
}