package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ResponseStatus(PRECONDITION_REQUIRED)
public class PreconditionRequiredException extends RuntimeException {
	public PreconditionRequiredException(String message) {
		super(message);
	}
	public PreconditionRequiredException(String message, Throwable cause) {
        super(message, cause);
    }
	public PreconditionRequiredException(Throwable cause) {
        super(cause);
    }
	public PreconditionRequiredException() {
		super();
	}
}
