package com.sigeosrl.exceptionandlog.exception.service;

import com.sigeosrl.exceptionandlog.exception.*;
import com.sigeosrl.exceptionandlog.exception.dto.ProdErrorMessage;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
@Profile("prod")
public class ProdExceptionHandler {
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(BAD_REQUEST)
    public ProdErrorMessage handleBadRequestException(BadRequestException ex) {
        return new ProdErrorMessage(BAD_REQUEST, ex.getMessage(), BAD_REQUEST.value());
    }

    @ExceptionHandler(ConflictException.class)
    @ResponseStatus(CONFLICT)
    public ProdErrorMessage handleConflictException(ConflictException ex) {
        return new ProdErrorMessage(CONFLICT, ex.getMessage(), CONFLICT.value());
    }

    @ExceptionHandler(ExpectationFailedException.class)
    @ResponseStatus(EXPECTATION_FAILED)
    public ProdErrorMessage handleExpectationFailedException(ExpectationFailedException ex) {
        return new ProdErrorMessage(EXPECTATION_FAILED, ex.getMessage(), EXPECTATION_FAILED.value());
    }

    @ExceptionHandler(FailedDependencyException.class)
    @ResponseStatus(FAILED_DEPENDENCY)
    public ProdErrorMessage handleFailedDependencyException(FailedDependencyException ex) {
        return new ProdErrorMessage(FAILED_DEPENDENCY, ex.getMessage(), FAILED_DEPENDENCY.value());
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(FORBIDDEN)
    public ProdErrorMessage handleForbiddenException(ForbiddenException ex) {
        return new ProdErrorMessage(FORBIDDEN, ex.getMessage(), FORBIDDEN.value());
    }

    @ExceptionHandler(GoneException.class)
    @ResponseStatus(GONE)
    public ProdErrorMessage handleGoneException(GoneException ex) {
        return new ProdErrorMessage(GONE, ex.getMessage(), GONE.value());
    }

    @ExceptionHandler(LengthRequiredException.class)
    @ResponseStatus(LENGTH_REQUIRED)
    public ProdErrorMessage handleLengthRequiredException(LengthRequiredException ex) {
        return new ProdErrorMessage(LENGTH_REQUIRED, ex.getMessage(), LENGTH_REQUIRED.value());
    }

    @ExceptionHandler(LockedException.class)
    @ResponseStatus(LOCKED)
    public ProdErrorMessage handleLockedException(LockedException ex) {
        return new ProdErrorMessage(LOCKED, ex.getMessage(), LOCKED.value());
    }

    @ExceptionHandler(NotAcceptableException.class)
    @ResponseStatus(NOT_ACCEPTABLE)
    public ProdErrorMessage handleNotAcceptableException(NotAcceptableException ex) {
        return new ProdErrorMessage(NOT_ACCEPTABLE, ex.getMessage(), NOT_ACCEPTABLE.value());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ProdErrorMessage handleNotFoundException(NotFoundException ex) {
        return new ProdErrorMessage(NOT_FOUND, ex.getMessage(), NOT_FOUND.value());
    }

    @ExceptionHandler(PaymentRequiredException.class)
    @ResponseStatus(PAYMENT_REQUIRED)
    public ProdErrorMessage handlePaymentRequiredException(PaymentRequiredException ex) {
        return new ProdErrorMessage(PAYMENT_REQUIRED, ex.getMessage(), PAYMENT_REQUIRED.value());
    }

    @ExceptionHandler(PreconditionFailedException.class)
    @ResponseStatus(PRECONDITION_FAILED)
    public ProdErrorMessage handlePreconditionFailedException(PreconditionFailedException ex) {
        return new ProdErrorMessage(PRECONDITION_FAILED, ex.getMessage(), PRECONDITION_FAILED.value());
    }

    @ExceptionHandler(PreconditionRequiredException.class)
    @ResponseStatus(PRECONDITION_REQUIRED)
    public ProdErrorMessage handlePreconditionRequiredException(PreconditionRequiredException ex) {
        return new ProdErrorMessage(PRECONDITION_REQUIRED, ex.getMessage(), PRECONDITION_REQUIRED.value());
    }

    @ExceptionHandler(TeapotException.class)
    @ResponseStatus(I_AM_A_TEAPOT)
    public ProdErrorMessage handleTeapotException(TeapotException ex) {
        return new ProdErrorMessage(I_AM_A_TEAPOT, ex.getMessage(), I_AM_A_TEAPOT.value());
    }

    @ExceptionHandler(UnprocessableEntityException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public ProdErrorMessage handleUnprocessableEntityException(UnprocessableEntityException ex) {
        return new ProdErrorMessage(UNPROCESSABLE_ENTITY, ex.getMessage(), UNPROCESSABLE_ENTITY.value());
    }

    @ExceptionHandler(UnsupportedMediaTypeException.class)
    @ResponseStatus(UNSUPPORTED_MEDIA_TYPE)
    public ProdErrorMessage handleUnsupportedMediaTypeException(UnsupportedMediaTypeException ex) {
        return new ProdErrorMessage(UNSUPPORTED_MEDIA_TYPE, ex.getMessage(), UNSUPPORTED_MEDIA_TYPE.value());
    }
}