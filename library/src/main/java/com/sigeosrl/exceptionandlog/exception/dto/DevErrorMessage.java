package com.sigeosrl.exceptionandlog.exception.dto;

import org.springframework.http.HttpStatus;

public record DevErrorMessage(
	HttpStatus status,
	String message,
	StackTraceElement[] stackTrace,
	Integer originalStatus
) {}
