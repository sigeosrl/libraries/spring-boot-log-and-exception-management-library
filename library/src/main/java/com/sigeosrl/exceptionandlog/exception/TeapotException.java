package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.I_AM_A_TEAPOT;

@ResponseStatus(I_AM_A_TEAPOT)
public class TeapotException extends RuntimeException {
	public TeapotException(String message) {
		super(message);
	}
	public TeapotException(String message, Throwable cause) {
        super(message, cause);
    }
	public TeapotException(Throwable cause) {
        super(cause);
    }
	public TeapotException() {
        super();
    }
}
