package com.sigeosrl.exceptionandlog.exception.dto;

import org.springframework.http.HttpStatus;

public record ProdErrorMessage(
	HttpStatus status,
	String message,
	Integer originalStatus
) {}
