package com.sigeosrl.exceptionandlog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.FAILED_DEPENDENCY;

@ResponseStatus(FAILED_DEPENDENCY)
public class FailedDependencyException extends RuntimeException {
	public FailedDependencyException(String message) {
		super(message);
	}
	public FailedDependencyException(String message, Throwable cause) {
		super(message, cause);
	}
	public FailedDependencyException(Throwable cause) {
        super(cause);
    }
	public FailedDependencyException() {
        super();
    }
}
