package com.sigeosrl.exceptionandlog.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/changelog")
public class ChangeLogController {
	@GetMapping("/downlaod")
	public ResponseEntity<?> getChangelog() {
		// Accessing CHANGELOG.md from the classpath (inside src/main/resources)
		InputStream in = getClass().getResourceAsStream("/CHANGELOG.md");

		// Check if the input stream is null, which means the file was not found
		if (in == null) {
			// Return a 404 Not Found response if the file cannot be found
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("CHANGELOG.md file not found");
		}

		try {
			// Read the entire stream into a byte array
			byte[] content = in.readAllBytes();
			// Return the byte array with a response type of application/octet-stream
			return ResponseEntity
					.status(HttpStatus.OK)
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header("Content-Disposition", "attachment; filename=\"CHANGELOG.md\"")
					.body(content);
		} catch (Exception e) {
			// Return a 500 Internal Server Error response if an exception occurs while reading the file
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error reading CHANGELOG.md: " + e.getMessage());
		} finally {
			try {
				in.close();  // Ensure the InputStream is closed after use
			} catch (Exception e) {
				// Log or handle the failure to close the InputStream, if necessary
			}
		}
	}

	@GetMapping
	public ResponseEntity<String> getChangelogAsString() {
		// Accessing CHANGELOG.md from the classpath (inside src/main/resources)
		InputStream in = getClass().getResourceAsStream("/CHANGELOG.md");

		// Check if the input stream is null, which means the file was not found
		if (in == null) {
			// Return a 404 Not Found response if the file cannot be found
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("CHANGELOG.md file not found");
		}

		try {
			// Read the entire stream into a byte array
			byte[] content = in.readAllBytes();
			// Convert the byte array to a string using UTF-8 encoding
			String changelog = new String(content, StandardCharsets.UTF_8);
			// Return the string with a response type of text/plain
			return ResponseEntity
					.status(HttpStatus.OK)
					.contentType(MediaType.TEXT_PLAIN)
					.body(changelog);
		} catch (Exception e) {
			// Return a 500 Internal Server Error response if an exception occurs while reading the file
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error reading CHANGELOG.md: " + e.getMessage());
		} finally {
			try {
				in.close();  // Ensure the InputStream is closed after use
			} catch (Exception e) {
				// Log or handle the failure to close the InputStream, if necessary
			}
		}

	}
}
