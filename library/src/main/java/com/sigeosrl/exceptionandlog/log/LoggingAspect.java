package com.sigeosrl.exceptionandlog.log;


import com.sigeosrl.exceptionandlog.exception.*;
import com.sigeosrl.exceptionandlog.security.SecurityUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.Instant;

import static java.util.Objects.nonNull;

@Aspect
@Component
public class LoggingAspect {
    private Instant start;
    private boolean isAfterThrowing = false;

    @Autowired
    SecurityUtil<?> securityUtil;

    @Before("@annotation(LoggingBefore)")
    public void specificLogBefore(JoinPoint joinPoint) {
        isAfterThrowing = false;

        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getTarget().getClass().getSimpleName();
        Log.logStart(className, methodName, securityUtil.username());
        start = Instant.now();
    }
    
    @Before("@annotation(Logging)")
    public void logBefore(JoinPoint joinPoint) {
        specificLogBefore(joinPoint);
    }

    @After("@annotation(LoggingAfter)")
    public void specificLogAfter(JoinPoint joinPoint) {
       if (!isAfterThrowing) {
           String methodName = joinPoint.getSignature().getName();
           String className = joinPoint.getTarget().getClass().getSimpleName();
           Log.logEnd(className, methodName, securityUtil.username(), start);
       }
    }
    
    @After("@annotation(Logging)")
    public void logAfter(JoinPoint joinPoint) {
        specificLogAfter(joinPoint);
    }

    @AfterThrowing(pointcut = "@annotation(LoggingAfterThrowing)", throwing = "e")
    public void specificLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
        isAfterThrowing = true;

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        String className = joinPoint.getTarget().getClass().getSimpleName();
        LoggingAfterThrowing loggingAfterThrowing = method.getAnnotation(LoggingAfterThrowing.class);
        if (nonNull(loggingAfterThrowing)) {
            String methodName = joinPoint.getSignature().getName();
            Log.logException(className, methodName, securityUtil.username(), String.valueOf(checkStatus(e)), start, e);
        }
    }

    @AfterThrowing(pointcut = "@annotation(Logging)", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        isAfterThrowing = true;

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        String className = joinPoint.getTarget().getClass().getSimpleName();
        Logging loggingAfterThrowing = method.getAnnotation(Logging.class);
        if (nonNull(loggingAfterThrowing)) {
            String methodName = joinPoint.getSignature().getName();
            Log.logException(className, methodName, securityUtil.username(), String.valueOf(checkStatus(e)), start, e);
        }
    }

    private HttpStatus checkStatus(Throwable e) {
        return switch (e) {
            case BadRequestException ignored -> HttpStatus.BAD_REQUEST;
            case ConflictException ignored -> HttpStatus.CONFLICT;
            case ExpectationFailedException ignored -> HttpStatus.EXPECTATION_FAILED;
            case FailedDependencyException ignored -> HttpStatus.FAILED_DEPENDENCY;
            case ForbiddenException ignored -> HttpStatus.FORBIDDEN;
            case GoneException ignored -> HttpStatus.GONE;
            case LengthRequiredException ignored -> HttpStatus.LENGTH_REQUIRED;
            case LockedException ignored -> HttpStatus.LOCKED;
            case NotAcceptableException ignored -> HttpStatus.NOT_ACCEPTABLE;
            case NotFoundException ignored -> HttpStatus.NOT_FOUND;
            case PaymentRequiredException ignored -> HttpStatus.PAYMENT_REQUIRED;
            case PreconditionFailedException ignored -> HttpStatus.PRECONDITION_FAILED;
            case PreconditionRequiredException ignored -> HttpStatus.PRECONDITION_REQUIRED;
            case TeapotException ignored -> HttpStatus.I_AM_A_TEAPOT;
            case UnprocessableEntityException ignored -> HttpStatus.UNPROCESSABLE_ENTITY;
            case UnsupportedMediaTypeException ignored -> HttpStatus.UNSUPPORTED_MEDIA_TYPE;
            default -> HttpStatus.INTERNAL_SERVER_ERROR;
        };
    }
}