package com.sigeosrl.exceptionandlog.log;

import com.sigeosrl.exceptionandlog.util.StackTraceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ansi.AnsiBackground;
import org.springframework.boot.ansi.AnsiColor;

import java.time.*;
import java.time.format.DateTimeFormatter;

@Slf4j
public abstract class Log {
    private Log() {}

    public static void logStart(String clazz, String method, String username) {
        Instant start = Instant.now();
        OffsetDateTime zdtStart = start.atOffset(ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        String formattedStart = zdtStart.format(formatter);
        log.info("Method\033[3m {}\033[0m in class\033[3m {}\033[0m, called by: {}, started at: {}", method, clazz, username, formattedStart);
    }

    public static void logEnd(String clazz, String method, String username, Instant start) {
        Instant end = Instant.now();
        OffsetDateTime zdtEnd = end.atOffset(ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        String formattedEnd = zdtEnd.format(formatter);
        log.info("Method\033[3m {}\033[0m in class\033[3m {}\033[0m, called by: {}, ended at:   {}, elapsed time: {}",
                method,
                clazz,
                username,
                formattedEnd,
                Duration.ofMillis(end.toEpochMilli() - start.toEpochMilli())
        );

    }

    public static void logException(String clazz, String method, String username, String status, Instant start , Throwable e) {
        Instant end = Instant.now();
        OffsetDateTime zdtEnd = end.atOffset(ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
        String formattedEnd = zdtEnd.format(formatter);

        String stackTrace = StackTraceUtil.getPrettyStackTrace(e);
        int index = -1;
        for (int i = 0; i < stackTrace.length(); i++) {
            if (Character.isUpperCase(stackTrace.charAt(i))) {
                index = i;
                break;
            }
        }

        log.error("Method\033[3m {}\033[0m in class\033[3m {}\033[0m, called by: {}, ended at:   {}, elapsed time: {}, status: \033[41;30m{}\033[0m with exception: {}\n\033[31m{}\033[0m",
                method,
                clazz,
                username,
                formattedEnd,
                Duration.ofMillis(end.toEpochMilli() - start.toEpochMilli()),
                status,
                stackTrace.substring(index == -1 ? 0 : index, stackTrace.indexOf("\n")),
                stackTrace.substring(index == -1 ? 0 : index)
        );
    }
}
