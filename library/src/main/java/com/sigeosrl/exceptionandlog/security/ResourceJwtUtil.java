package com.sigeosrl.exceptionandlog.security;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.jwk.source.ImmutableSecret;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Slf4j
public class ResourceJwtUtil implements JwtUtil {

    static final String FENCED = "FENCED";
    static final String FENCE_ID = "FENCE_ID";

    @Value("${security.rolesFrom}")
    private String rolesFrom;

    @Override
    public UsernamePasswordAuthenticationToken parseToken(String token) throws ParseException {

        SignedJWT signedJWT = SignedJWT.parse(token);
        JWTClaimsSet claims = signedJWT.getJWTClaimsSet();
        String username = claims.getClaim("preferred_username").toString();

        List<String> fence = (List<String>)claims.getClaim("fence");
        boolean fenced = fence != null && !fence.isEmpty() && !fence.contains("0");
        Long fenceId = fenced ? Long.parseLong(fence.get(0)) : 0;

        var user =  new UsernamePasswordAuthenticationToken(username, null, authorities(claims));
        user.setDetails(Map.of(FENCED, fenced, FENCE_ID, fenceId));

        return user;
    }

    @Override
    public UsernamePasswordAuthenticationToken parseToken(String token, String secret) throws ParseException, BadJOSEException, JOSEException {

        verifyToken(token, secret);

        return parseToken(token);
    }

    private void verifyToken(String token, String secret) throws ParseException, JOSEException, BadJOSEException {

        SignedJWT signedJWT = SignedJWT.parse(token);

        byte[] secretKey = secret.getBytes();
        signedJWT.verify(new MACVerifier(secretKey));

        ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();

        JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<>(JWSAlgorithm.HS256, new ImmutableSecret<>(secretKey));
        jwtProcessor.setJWSKeySelector(keySelector);
        jwtProcessor.process(signedJWT, null);

    }


    @SuppressWarnings("unchecked")
    private List<SimpleGrantedAuthority> authorities(JWTClaimsSet claims) {

        log.debug("claims: {}", claims);

        Map<String, Object> raccess = (Map<String, Object>)claims.getClaim("resource_access");
        Map<String, Object> beRoles = (Map<String, Object>)raccess.get(rolesFrom);
        Collection<String> roles = (Collection<String>)beRoles.get("roles");

        return roles == null ? null : roles.stream()
                .map(role -> role.replace("CFAA-", ""))
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

}