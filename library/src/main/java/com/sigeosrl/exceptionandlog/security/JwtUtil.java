package com.sigeosrl.exceptionandlog.security;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.BadJOSEException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.text.ParseException;

public interface JwtUtil {
    UsernamePasswordAuthenticationToken parseToken(String token) throws ParseException;
    UsernamePasswordAuthenticationToken parseToken(String token, String secret) throws ParseException, BadJOSEException, JOSEException;
}