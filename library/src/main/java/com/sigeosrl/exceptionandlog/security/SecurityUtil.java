package com.sigeosrl.exceptionandlog.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Component
public class SecurityUtil<E> {

    public Authentication authentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public String username() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public Set<String> getRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(toSet());
    }

    public E getUser() {
        return null;
    }

}
