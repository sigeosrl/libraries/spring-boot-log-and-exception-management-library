# Spring Boot Log and Exception Management Library

Followed the instructions from [here](https://docs.gitlab.com/ee/user/packages/maven_repository/?tab=gradle).

## Pre-requisites

in `~/.gradle/gradle.properties` add the following:

```gitLabPrivateToken=YOURTOKEN```

For create a `TOKEN` you can follow [this](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

## Library

1. Add
```id 'maven-publish'``` in plugins section

2. Add
```
publishing {
	publications {
		mavenJava(MavenPublication) {
			versionMapping {
				usage('java-api') {
					fromResolutionOf('runtimeClasspath')
				}
				usage('java-runtime') {
					fromResolutionResult()
				}
			}
		}
	}
	repositories {
		maven {
			url "https://gitlab.com/api/v4/projects/YOURPROJECTID/packages/maven"
			credentials(HttpHeaderCredentials) {
				name = "Private-Token"
				value = gitLabPrivateToken // the variable resides in $GRADLE_USER_HOME/gradle.properties
			}
			authentication {
				header(HttpHeaderAuthentication)
			}
		}
	}
}
```
And substitute `YOURPROJECTID` with the project id of your project.

## Demo Project

1. Add
```
    maven {
		url 'https://gitlab.com/api/v4/projects/55038248/packages/maven'
	}
```
to `repositories` section and substitute `YOURPROJECTID` with the project id of your project.


### Enjoy :)