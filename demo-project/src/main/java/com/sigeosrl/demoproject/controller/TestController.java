package com.sigeosrl.demoproject.controller;

import com.sigeosrl.exceptionandlog.exception.UnprocessableEntityException;
import com.sigeosrl.exceptionandlog.log.Logging;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @GetMapping("/exception")
    @Logging
    ResponseEntity<String> helloException() throws Exception {
        throw new UnprocessableEntityException("This is a test exception!");
    }

    @GetMapping("/normal")
    @ResponseStatus(NO_CONTENT)
    @Logging
    void helloNormal() {
    }

    @GetMapping("/no-exception")
    @ResponseStatus(OK)
    @Logging
    String helloNoException() {
        return "Hello World!";
    }
}
